﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ouvrirPause : MonoBehaviour
{
    public GameObject EcranPause;

    public void montrerPause()
    {
        //Si on appuie sur le bouton pause alors 
        if (EcranPause != null)
        {
            //On freeze le jeu
            Time.timeScale = 0.0f;
            //On met à true la variable booléenne
            bool estActif = EcranPause.activeSelf;
            //On active le panneau de pause
            EcranPause.SetActive(!estActif);
            //Si le joueur appuie de nouveau sur le bouton alors
            if (estActif)
            {
                //On defreeze le jeu
                Time.timeScale = 1.0f;
            }
        }
    }
}
