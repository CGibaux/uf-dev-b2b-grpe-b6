﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;



public class mouvementBalle : MonoBehaviour
{
    //Variables globales
    public Rigidbody2D rb;
    public bool enJeu;
    public Transform paddle, explosionTerre, explosionBois, explosionPierre, explosionPierreLit;
    public Transform PouvoirVieSup, PouvoirVieMoins, PouvoirX2, PouvoirAgrandissementPaddle, PouvoirRetrecissementPaddle, PouvoirPaddleRapide, PouvoirPaddleLent, PouvoirBalleGeante, PouvoirBalleMinuscule;
    public float vitesse, taille;
    //Instancier l'audio du jeu
    AudioSource audio;
    //Les différents bruitages
    public AudioClip murRebonds, fissureBriques, paddleSon, defaite;
    public mouvementPaddle pad;
    public GameManager gm;
    public GameObject balle;
    
    //Start is called before the first frame update
    void Start()
    {
        //Component du RigidBody
        rb = GetComponent<Rigidbody2D>();
        //Component de l'audio
        audio = GetComponent<AudioSource>();
        
    }
    //Update is called once per frame
    void Update()
    {
        //Si c'est la fin du jeu alors la variable booléenne passe à true
        //ANCIEN TEST
        /*if (gm.finDePartie)
        {
            //La balle prend la position paddle
            transform.position = paddle.position;
            //On lui enlève sa force
            rb.velocity = Vector2.zero;
            //Dès que le joueur appuie sur la barre espace alors
            if (Input.GetButtonDown("Jump"))
            {
                //On donne de la force à la balle
                rb.AddForce(Vector2.up * vitesse);
                //Continuer le jeu
                gm.finDePartie = false;
            }
            return;
        }*/
        //Si la variable enJeu est différente de 'True' alors
        if (!enJeu)
        {
            //La balle prend la position paddle
            transform.position = paddle.position;
        }
        //POUR WINDOWS
        //Si la barre d'espace est préssée et que la variable enJeu = false alors
        if (Input.GetButtonDown("Jump") && !enJeu)
        {
            //Variable enJeu = true
            enJeu = true;
            //Ajouter une force à la balle
            rb.AddForce(Vector2.up * vitesse);
        }
        
        //POUR ANDROID
        //Si le joueur touche l'écran alors, la variable enJeu = false alors
        /*if (Input.GetMouseButtonDown(0) && !enJeu)
        {
            //Variable enJeu = true
            enJeu = true;
            //Ajouter une force à la balle
            rb.AddForce(Vector2.up * vitesse);
        }*/
        
        //Si le booléen balleGeante est true alors
        if (pad.balleGeante)
        {
            //La taille de la balle est multiplié par 4
            balle.transform.localScale = new Vector2((taille * 4), (taille * 4));

        }
        //Si le booléen balleMinuscule est true alors
        else if (pad.balleMinuscule)
        {
            //La taille de la balle est divisé par 2
            balle.transform.localScale = new Vector2((taille / 2), (taille / 2));

        }
        //Sinon
        else
        {
            //La taille de la balle est normale
            balleTailleNormal();
        }
    }

    //Fonction quand la balle touche le mur en bas (Sortie de l'écran)
    void OnTriggerEnter2D(Collider2D other)
    {
        //Si la balle touche le MurBas alors
        if (other.CompareTag("murBas"))
        {
            //Retirer la force de la balle
            rb.velocity = Vector2.zero;
            //Retirer une vie
            gm.MajVies(-1);
            if (gm.vies == 0)
            {
                //Stop la musique de fon
                audio.Stop();
                Debug.Log("la musique de fond est morte normalement");
                //Emmet le son "defaite"
                audio.clip = defaite;
                audio.Play();
            }
            //Variable en jeu = false
            enJeu = false;
        }
    }

    //Fonction qui s'active quand la balle touche un collider qui n'est pas le sien
    void OnCollisionEnter2D(Collision2D other)
    {
        //Checker si ce que la balle touche est une brique
        if (other.transform.CompareTag("brique1Coup") || other.transform.CompareTag("brique2Coup") || other.transform.CompareTag("brique3Coup") || other.transform.CompareTag("brique4Coup"))
        {
            //Créer une variable qui stocke les briques
            briquesScript briqueScript = other.gameObject.GetComponent<briquesScript>();
            //Si la brique se prend un coup alors
            if (briqueScript.coupDestruction > 1)
            {
                //Emmet le son "fissureBriques"
                audio.clip = fissureBriques;
                audio.Play();
                //Appeler la fonction pour casser la brique
                briqueScript.CasserBrique();
            }
            //Sinon
            else
            {
                //Génération d'un nombre aléatoire
                int chanceAleatoire = Random.Range(1, 150);
                //Si ce nombre est inférieur à 50
                if (chanceAleatoire < 4)
                {
                    //Un PouvoirVieSup apparaît
                    Instantiate(PouvoirVieSup, other.transform.position, other.transform.rotation);
                }
                else if (chanceAleatoire < 7)
                {
                    //Un PouvoirVieMalus apparaît
                    Instantiate(PouvoirVieMoins, other.transform.position, other.transform.rotation);
                }
                else if (chanceAleatoire < 12)
                {
                    //Un X2Points apparaît
                    Instantiate(PouvoirX2, other.transform.position, other.transform.rotation);
                }
                else if (chanceAleatoire < 15)
                {
                    //Le pouvoir "agrandissementPaddle" apparaît
                    Instantiate(PouvoirAgrandissementPaddle, other.transform.position, other.transform.rotation);
                }
                else if (chanceAleatoire < 18)
                {
                    //Le pouvoir "retrecissementPaddle" apparaît
                    Instantiate(PouvoirRetrecissementPaddle, other.transform.position, other.transform.rotation);
                }
                else if (chanceAleatoire < 21)
                {
                    //Le pouvoir "paddleRapide" apparaît
                    Instantiate(PouvoirPaddleRapide, other.transform.position, other.transform.rotation);
                }
                else if (chanceAleatoire < 24)
                {
                    //Le pouvoir "paddleLent" apparaît
                    Instantiate(PouvoirPaddleLent, other.transform.position, other.transform.rotation);
                }
                else if (chanceAleatoire < 27)
                {
                    //Le pouvoir "balleGéante" apparaît
                    Instantiate(PouvoirBalleGeante, other.transform.position, other.transform.rotation);
                }
                else if (chanceAleatoire < 30)
                {
                    //Le pouvoir "balleMinuscule" apparaît
                    Instantiate(PouvoirBalleMinuscule, other.transform.position, other.transform.rotation);
                }

                if (other.transform.CompareTag("brique1Coup"))
                {
                    // Instancier l'explosion du bloc terre et le faire exploser
                    Transform nouvelleExplosionTerre = Instantiate(explosionTerre, other.transform.position, other.transform.rotation);
                    // Faire disparaître les particules d'explosion de la scène afin d'optimiser la mémoire
                    Destroy(nouvelleExplosionTerre.gameObject, 2f);
                }
                else if (other.transform.CompareTag("brique2Coup"))
                {
                    // Instancier l'explosion du bloc terre et le faire exploser
                    Transform nouvelleExplosionBois = Instantiate(explosionBois, other.transform.position, other.transform.rotation);
                    // Faire disparaître les particules d'explosion de la scène afin d'optimiser la mémoire
                    Destroy(nouvelleExplosionBois.gameObject, 2f);
                }
                else if (other.transform.CompareTag("brique3Coup"))
                {
                    // Instancier l'explosion du bloc terre et le faire exploser
                    Transform nouvelleExplosionPierre = Instantiate(explosionPierre, other.transform.position, other.transform.rotation);
                    // Faire disparaître les particules d'explosion de la scène afin d'optimiser la mémoire
                    Destroy(nouvelleExplosionPierre.gameObject, 2f);
                }
                else if (other.transform.CompareTag("brique4Coup"))
                {
                    // Instancier l'explosion du bloc terre et le faire exploser
                    Transform nouvelleExplosionPierreLit = Instantiate(explosionPierreLit, other.transform.position, other.transform.rotation);
                    // Faire disparaître les particules d'explosion de la scène afin d'optimiser la mémoire
                    Destroy(nouvelleExplosionPierreLit.gameObject, 2f);
                }

                //Si le paddle touche l'icone du X2 points alors
                if (pad.x2points)
                {
                    //On ajout le double de points que l'on gagne de base sur la brique détruite
                    gm.MajPoints(briqueScript.points * 2);
                }
                else
                {
                    //Ajout des points
                    gm.MajPoints(briqueScript.points);
                }
                //Mettre à jour le nombre de briques qu'il y a actuellement sur la scène Unity
                gm.MajNombreBriques();
                //Faire disparaître la brique
                Destroy(other.gameObject);
            }
            
        }
        //Si la balle touche le mur gauche, droit ou haut alors
        else if (other.transform.CompareTag("murRebonds"))
        {
            //Emmet le son "murRebonds"
            audio.clip = murRebonds;
            //Activation de l'audio
            audio.Play();
        }
        //Si la balle touche le mur gauche, droit ou haut alors
        else if (other.transform.CompareTag("paddle"))
        {
            //Emmet le son "paddle"
            audio.clip = paddleSon;
            //Activation de l'audio
            audio.Play();
        }
    }
    
    //Fonction pour faire revenir le paddle à une taille normale
    public void balleTailleNormal()
    {
        //On faite revenir le paddle à une taille normale
        balle.transform.localScale = new Vector2(taille, taille);
    }
}
