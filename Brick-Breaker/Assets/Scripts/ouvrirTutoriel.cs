﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ouvrirTutoriel : MonoBehaviour
{
    public void ouvrirTuto()
    {
        //Chargement de la scène "Tuto"
        SceneManager.LoadScene("Tuto");
    }
}
