﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Variables globles
    public int points, vies, nombreDeBriques, niveauActuel = 0;
    //Variables textes des points, des vies et du meilleur score
    public Text affichagePoints, affichageVies, affichageTopScore;
    //Variable booléen fin du jeu
    public bool finDePartie;
    //Se me à true si le joueur a choisi un niveau 
    private bool ChoixJoueur = false;
    //Variable qui affiche le menu de fin du jeu
    public GameObject panneauFinDuJeu, ecranChargementNiveau;
    //Le tableau qui contient toutes les images de fond de niveau
    public Sprite[] fondNiveau;
    //Sprite du niveau en cours
    public SpriteRenderer spriteFondNiveau;
    //Tableau contenant les niveaux
	public Transform[] niveaux;
    //Champs Input du pseudo
    public InputField topScoreInput;
    //Instancier l'audio du jeu
    AudioSource audio;
    public AudioClip defaiteSon, victoireSon;
    //Start is called before the first frame update
    void Start()
    {
        //Initialisation de l'affichage des vies
        affichageVies.text = "Vies : " + vies;
        //Initialisation de l'affichage des Points
        affichagePoints.text = "Points : " + points;
        //Calcul du nombre de briques par Unity
        nombreDeBriques = GameObject.FindGameObjectsWithTag("brique1Coup").Length;
        //Calcul du nombre de briques par Unity
        nombreDeBriques += GameObject.FindGameObjectsWithTag("brique2Coup").Length;
        //Calcul du nombre de briques par Unity
        nombreDeBriques += GameObject.FindGameObjectsWithTag("brique3Coup").Length;
        //Calcul du nombre de briques par Unity
        nombreDeBriques += GameObject.FindGameObjectsWithTag("brique4Coup").Length;
        //Instanciation de la balle
        gameObject.GetComponent<mouvementBalle>();
        //Instanciation de l'audio
        audio = GetComponent<AudioSource>();
        
        //Si le joueur choisi un niveau alors
        if (PlayerPrefs.GetInt("ChoixPlayer") != 0)
        {
            //On passe la variable à true
            ChoixJoueur = true;
            //On met à jour les briques du niveau
            MajNombreBriques();
        }

    }

    //Fonction qui modifie le nombre de vies
    public void MajVies(int changementVie)
    {
        //Ajout de la nouvelle valeur
        vies += changementVie;
        //Si le joueur n'a plus de vie alors
        if (vies <= 0)
        {
            //On instancie les vies à zero
            vies = 0;
            //On freeze le jeu pour empêcher le joueur de continuer de jouer après sa défaite
            Time.timeScale = 0.0f;
            //On attribue le message défaite à une variable
            string defaiteMessage = "Vous n'avez plus de vies ¯\\_(ツ)_/¯";
            //Fonction qui active le panneau de défaite
            FinDuJeu(defaiteMessage);
            //On instancie le son de défaite
            audio.clip = defaiteSon;
            //Et on active ce son
            audio.Play();
        }
        //Affichage de la nouvelle valeur
        affichageVies.text = "Vies : " + vies;
    }
    
    //Fonction qui modifie le nombre de points
    public void MajPoints(int changementPoints)
    {
        //Ajout de la nouvelle valeur
        points += changementPoints;
        //Affichage de la nouvelle valeur
        affichagePoints.text = "Points : " + points;

    }
    
    public void MajNombreBriques()
    {
        nombreDeBriques--;
        //S'il n'y a plus de briques
        if (nombreDeBriques <= 0 || ChoixJoueur)
        {
            //Si le niveaux actuel est supérieur ou égal au dernier niveau -1 (== Dernier niveau)
            if (niveauActuel >= niveaux.Length - 1)
            {
                //Freeze le jeu pour empêcher le joueur de continuer de jouer après sa victoire
                Time.timeScale = 0.0f;
                //Afficher le menu fin de jeu
                string victoireMessage = "Vous avez terminé le jeu (●'◡'●)";
                //Son de victoire du jeu
                audio.clip = victoireSon;
                //Et on active ce son
                audio.Play();
                FinDuJeu(victoireMessage);
            }
            //Sinon
            else
            {
                //Si le joueur à choisi un niveau alors
                if (ChoixJoueur)
                {
                    //On récupère la variable du niveau 
                    niveauActuel = PlayerPrefs.GetInt("ChoixPlayer") - 2;
                    //On repasse la variable à false pour éviter de recharger le niveau en boucle
                    ChoixJoueur = false;
                }
                //Ecran fin de niveau
                ecranChargementNiveau.SetActive(true);
                //Le texte affiché sur l'écran de fin de niveau
                ecranChargementNiveau.GetComponentInChildren<Text>().text = "Chargement Niveau " + (niveauActuel + 2) + " ╰(°▽°)╯";
                //Changement du sprite de fond
                spriteFondNiveau.sprite = fondNiveau[niveauActuel + 1];
                //On passe à true la variable afin de changer de niveau
                finDePartie = true;
                //Niveau suivant
                Invoke("ChargementNiveau", 1f);
            }
        }
    }

    void ChargementNiveau()
    {
        //Ajout d'un niveau à l'actuel
        niveauActuel++;
        //Chargement du premier niveau si le joueur décide de rejouer
        PlayerPrefs.SetInt("ChoixPlayer", niveauActuel + 1);
        //Niveau suivant dans le tableau
        Instantiate(niveaux[niveauActuel], Vector2.zero, Quaternion.identity);
        //Réactualiser le nombre de briques
        nombreDeBriques = GameObject.FindGameObjectsWithTag("brique1Coup").Length;
        //Réactualiser le nombre de briques
        nombreDeBriques += GameObject.FindGameObjectsWithTag("brique2Coup").Length;
        //Réactualiser le nombre de briques
        nombreDeBriques += GameObject.FindGameObjectsWithTag("brique3Coup").Length;
        //Réactualiser le nombre de briques
        nombreDeBriques += GameObject.FindGameObjectsWithTag("brique4Coup").Length;
        //Cacher l'écran fin de niveau
        ecranChargementNiveau.SetActive(false);
    }

    void FinDuJeu(string message)
    {
        //Passer la varible à true
        finDePartie = true;
        //Afficher le panneau de fin du jeu quand la varible passe à true
        panneauFinDuJeu.SetActive(true);
        //On affiche le texte sur le panneau
        panneauFinDuJeu.GetComponentInChildren<Text>().text = message;
        //Initialisation du meilleur score
        int topScore = PlayerPrefs.GetInt("TOPSCORE");
        //Si le score da la partie est supérieur au meilleur score
        if (points > topScore)
        {
            //Le meilleur score prend la valeur du score actuel
            PlayerPrefs.SetInt("TOPSCORE", points);
            //Affichage du nouveau meilleur score
            affichageTopScore.text = "Vous avez le record de points" + "\n" + "Quel est votre pseudo ?";
            //L'input du pseudo s'affiche
            topScoreInput.gameObject.SetActive(true);
        }
        else
        {
            //Affichage du meilleur score et du score actuel
            affichageTopScore.text = "Meilleur score : " + PlayerPrefs.GetString("TOPSCOREPSEUDO") + " / " + topScore + " points" + "\n" + "Votre score : " + points;
        }
    }
    

    public void NouveauTopScore()
    {
        //On créer une variable qui stockera notre meilleur score
        string topScorePseudo = topScoreInput.text;
        //On créer un string pour stocker le score
        PlayerPrefs.SetString("TOPSCOREPSEUDO",topScorePseudo);
        //On le met en false car il s'affichera que quand le joueur perd ou dans le menu principal
        topScoreInput.gameObject.SetActive(false);
        //On affiche le meilleur score actuel
        affichageTopScore.text = "Bravo " + topScorePseudo + "\n" + "Vous avez établi le record à " + points + " points";
    }


    public void RejouerLeNiveau()
    {
        //Recharger la scène "Main"
        SceneManager.LoadScene("Main");
        //On Défreeze le jeu une fois le boutton "RejouerLeNiveau" pressé
        Time.timeScale = 1.0f;
    }
    public void Rejouer()
    {
        //Chargement du premier niveau si le joueur décide de rejouer
        PlayerPrefs.SetInt("ChoixPlayer", 1);
        //Recharger la scène depuis le début
        SceneManager.LoadScene("Main");
        //On Défreeze le jeu une fois le boutton "Rejouer" pressé
        Time.timeScale = 1.0f;
    }

    public void retourMenuPrincipal()
    {
        //Revenir au menu principal
        SceneManager.LoadScene("MenuPrincipal");
        //On Défreeze le jeu une fois le boutton "Menu" pressé
        Time.timeScale = 1.0f;
    }

    public void quitterJeu()
    {
        //On quitte le jeu
        Application.Quit();
    }
}