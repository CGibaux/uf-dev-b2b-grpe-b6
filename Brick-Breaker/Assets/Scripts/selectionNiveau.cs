﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class selectionNiveau : MonoBehaviour
{
    public void selectionDesNiveaux(int choixNiveau)
    {
        //On instancie la variable "choixPlayer" au niveau choisi
        PlayerPrefs.SetInt("ChoixPlayer", choixNiveau);
        //Chargement de la scène "Main"
        SceneManager.LoadScene("Main");
    }

    public void revenirMenu()
    {
        //Chargement de la scène "MenuPrincipal"
        SceneManager.LoadScene("MenuPrincipal");
    }
}
