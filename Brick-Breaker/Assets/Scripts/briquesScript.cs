﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class briquesScript : MonoBehaviour
{
    public int points, coupDestruction;
    public Sprite[] coupSprite;

    //Fonction qui change l'état de la brique
    public void CasserBrique()
    {
        //Faire perdre un point de vie à la brique
        coupDestruction--;
        //Changer le sprite de la brique
        GetComponent<SpriteRenderer>().sprite = coupSprite[coupDestruction-1];

    }
}
