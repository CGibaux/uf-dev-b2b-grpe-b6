﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pouvoirs : MonoBehaviour
{
    //variables globales
    public float speed;

    // Update is called once per frame
    void Update()
    {
        //Vitesse à laquelle le pouvoir descend
        transform.Translate(new Vector2(0f, -1) * Time.deltaTime * speed);
        //Destruction de l'objet s'il n'est pas attrapé après 6secondes
        if (transform.position.y < -10f)
        {
            //Détruire l'objet
            Destroy(gameObject);
        }
    }
}
