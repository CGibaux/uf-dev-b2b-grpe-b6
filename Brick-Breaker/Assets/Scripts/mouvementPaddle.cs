﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class mouvementPaddle : MonoBehaviour
{
    public float vitesse, vitesseNormale, bordEcranDroit, bordEcranGauche, largeurPaddle, longueurPaddle;
    public GameManager gm;
    public mouvementBalle balle;
    public bool x2points, balleGeante, balleMinuscule, paddleGeant, paddleMinuscule;
    public GameObject iconeX2Points, paddle;
    private float tempPourX2;
    
    // Update is called once per frame
    void Update()
    {
        
        // variable qui récupèrent les touches q et d et la flèche de droite et de gauche afin de faire bouger le paddle 
        //POUR WINDOWS
        float horizontale = Input.GetAxis("Horizontal");
        //POUR ANDROID
        //float horizontale = CrossPlatformInputManager.GetAxis("Horizontal");
        // faire -1 quand le joueur appuie sur les touches pour aller à gauche et donc faire déplacer le paddle à gauche et inversemment
        // Time.deltaTime pour régler la vitesse et avoir la bonne vitesse
        transform.Translate(Vector2.right * horizontale * Time.deltaTime * vitesse);
        //Si le paddle est géant alors
        if (paddleGeant)
        {
            //Pour empêcher le paddle géant de sortir de la limite gauche de l'écran
            if (transform.position.x < bordEcranGauche + (largeurPaddle * 2) -1)
            {
                transform.position = new Vector2(bordEcranGauche + (largeurPaddle * 2) - 1, transform.position.y);
            }

            //Pour empêcher le paddle géant de sortir de la limite droite de l'écran
            if (transform.position.x > bordEcranDroit - (largeurPaddle * 2) +1)
            {
                transform.position = new Vector2(bordEcranDroit - (largeurPaddle * 2) + 1, transform.position.y);
            }
        }
        //Et Si le paddle est minuscule alors
        else if (paddleMinuscule)
        {
            //Pour empêcher le paddle minuscule de sortir de la limite gauche de l'écran
            if (transform.position.x < (bordEcranGauche - (largeurPaddle / 2)))
            {
                transform.position = new Vector2(bordEcranGauche - (largeurPaddle / 2), transform.position.y);
            }

            //Pour empêcher le paddle minuscule de sortir de la limite droite de l'écran
            if (transform.position.x > bordEcranDroit + (largeurPaddle / 2))
            {
                transform.position = new Vector2(bordEcranDroit + (largeurPaddle / 2), transform.position.y);
            }
        }
        //Sinon si le paddle est de taille normale alors
        else
        {
            //Pour empêcher le paddle normal de sortir de la limite gauche de l'écran
            if (transform.position.x < bordEcranGauche)
            {
                transform.position = new Vector2(bordEcranGauche, transform.position.y);
            }

            //Pour empêcher le paddle minuscule de sortir de la limite droite de l'écran
            if (transform.position.x > bordEcranDroit)
            {
                transform.position = new Vector2(bordEcranDroit, transform.position.y);
            }
        }
    }

    //Quand l'objet touche le paddle
    void OnTriggerEnter2D(Collider2D other)
    {
        //Si l'objet possède le tag plus1vie
        if (other.CompareTag("plus1vie"))
        {
            //Ajout d'une vie
            gm.MajVies(1);
        }
        //Si l'objet possède le tag moins1vie
        else if (other.CompareTag("moins1vie"))
        {
            //Suppression d'une vie
            gm.MajVies(-1);
        }
        //Si l'objet possède le tag X2Points
        else if (other.CompareTag("X2Points"))
        {
            //On active l'icone du X2 points quand on ramasse le bonus
            iconeX2Points.SetActive(true);
            //On active le doubleur de points
            x2points = true;
            //Au bout de 10 secondes, on revient à la normale
            Invoke("tempsX2Points", 10);
        }
        //Si le paddle touche le bonus "agrandissementPaddle" alors
        else if (other.CompareTag("agrandissementPaddle"))
        {
            //On active la fonction qui agrandit son paddle
            agrandissementPaddle();
        }
        //Si le paddle touche le bonus "retrecissementPaddle" alors
        else if (other.CompareTag("retrecissementPaddle"))
        {
            //On active la fonction qui retrecie son paddle
            retrecissementPaddle();
        }
        //Si le paddle touche le bonus "paddleRapide" alors
        else if (other.CompareTag("paddleRapide"))
        {
            //On active la fonction qui accélère son paddle
            paddleRapide();
        }
        //Si le paddle touche le bonus "paddleLent" alors
        else if (other.CompareTag("paddleLent"))
        {
            //On active la fonction qui ralenti son paddle
            paddleLent();
        }
        //Si le paddle touche le bonus "balleGeante" alors
        else if (other.CompareTag("balleGeante"))
        {
            //on active le pouvoir "balleGeante" en passant le booléen à true
            balleGeante = true;
            //On active la fonction qui grandi sa balle pendant 10 secondes uniquement
            Invoke("tempsBalleGeante", 10);

        }
        //Si le paddle touche le bonus "balleMinuscule" alors
        else if (other.CompareTag("balleMinuscule"))
        {
            //on active le pouvoir "balleMinuscule" en passant le booléen à true
            balleMinuscule = true;
            //On active la fonction qui retrécit sa balle pendant 10 secondes uniquement
            Invoke("tempsBalleMinuscule", 10);
        }
        //Détruire l'objet
        Destroy(other.gameObject);

    }
    
    //Fonction pour le pouvoir "X2Points"
    public void tempsX2Points()
    {
        //On désactive l'icone du X2 points quand on ramasse le bonus
        iconeX2Points.SetActive(false);
        //Ajout de X2 points
        x2points = false;
    }

    public void tempsBalleGeante()
    {
        //Une fois les 10 secondes écoulées on repasse le booléen à false
        balleGeante = false;
    }
    
    public void tempsBalleMinuscule()
    {
        //Une fois les 10 secondes écoulées on repasse le booléen à false
        balleMinuscule = false;
    }

    //Fonction pour le pouvoir "agrandissementPaddle"
    public void agrandissementPaddle()
    {
        //On multiplie la taille du paddle par 2
        paddle.transform.localScale = new Vector2((largeurPaddle * 2), longueurPaddle);
        //On passe le booléen à true
        paddleGeant = true;
        //Au bout de 5 secondes, on active la fonction "paddleTailleNormale" qui fait revenir le paddle à une taille normale
        Invoke("paddleTailleNormal", 10);
    }

    //Fonction pour le pouvoir "retrecissementPaddle"
    public void retrecissementPaddle()
    {
        //On divise la taille du paddle par 2
        paddle.transform.localScale = new Vector2((largeurPaddle / 2), longueurPaddle);
        //On passe le booléen à true
        paddleMinuscule = true;
        //Au bout de 5 secondes, on active la fonction "paddleTailleNormale" qui fait revenir le paddle à une taille normale
        Invoke("paddleTailleNormal", 10);
    }

    //Fonction pour faire revenir le paddle à une taille normale
    public void paddleTailleNormal()
    {
        //On faite revenir le paddle à une taille normale
        paddle.transform.localScale = new Vector2(largeurPaddle, longueurPaddle);
        ///On passe le booléen à false
        paddleGeant = false;
        ///On passe le booléen à false
        paddleMinuscule = false;
    }
    
    //Fonction pour le pouvoir "paddleRapide"
    public void paddleRapide()
    {
        //On prend la vitesse du paddle et on la multiplie par 2
        vitesse = vitesseNormale * 2;
        //Au bout de 5 secondes, on active la fonction "paddleVitesseNormale" qui fait revenir le paddle à une vitesse normale
        Invoke("paddleVitesseNormale", 5);
    }

    //Fonction pour le pouvoir "paddleLent"
    public void paddleLent()
    {
        //On prend la vitesse du paddle et on la divise par 2
        vitesse = vitesseNormale / 2;
        //Au bout de 5 secondes, on active la fonction "paddleVitesseNormale" qui fait revenir le paddle à une vitesse normale
        Invoke("paddleVitesseNormale", 5);
    }

    //On prend la vitesse du paddle et on la multiplie par 2
    public void paddleVitesseNormale()
    {
        //On dis que la vitesse reviens à la normale
        vitesse = vitesseNormale;
    }
}
