﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPrincipal : MonoBehaviour
{
   //Variable pour afficher le meilleur score
   public Text texteMeilleurScore;

   //Affichage du bloc texte pour le meilleur score
   public void Start()
   {
      //Si il n'y a pas de meilleurs score on affiche rien
      if (PlayerPrefs.GetString("TOPSCOREPSEUDO") != "")
      {
         //Si il y a un meilleur score alors on affiche le meilleur score actuel
         texteMeilleurScore.text = "Le meilleur score a été fait par : " + PlayerPrefs.GetString("TOPSCOREPSEUDO") + " / " + PlayerPrefs.GetInt("TOPSCORE");
      }
   }

   //Fonction qui permet de quitter le jeu
   public void quitterJeu()
   {
      //On quitte le jeu
      Application.Quit();
      //On vérifie dans la console que le jeu est bien quitté
      Debug.Log("Le jeu est quitté");
   }

   //Fonction qui permet de commencer le jeu
   public void commencerJeu()
   {
      //On charge la première scène du jeu
      PlayerPrefs.SetInt("ChoixPlayer", 1);
      //On charge la scène "Main"
      SceneManager.LoadScene("Main");
   }

   public void tuto()
   {
      //On charge la scène "Tuto"
      SceneManager.LoadScene("Tuto");
   }   
   public void revenirMenu()
   {
      //On charge la scène "MenuPrincipal"
      SceneManager.LoadScene("MenuPrincipal");
   }

   public void selectionNiveau()
   {
      //On charge la scène "SelectionNiveau"
      SceneManager.LoadScene("SelectionNiveau");
   }
}
